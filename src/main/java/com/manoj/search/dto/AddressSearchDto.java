package com.manoj.search.dto;

import lombok.Data;

@Data
public class AddressSearchDto {
	private String countryId;
	private String countryName;
	private String cityName;
	private String stateName;
	private String searchKey;
}
