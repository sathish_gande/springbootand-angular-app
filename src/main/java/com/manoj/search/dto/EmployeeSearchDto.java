package com.manoj.search.dto;

import lombok.Data;

@Data
public class EmployeeSearchDto {

	private String empId;
	private String empFirstName;
	private String empMidleName;
	private String emplLastName;
	private String empGender;
	private String searchKey;
}
