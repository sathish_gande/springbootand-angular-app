package com.manoj.search.dto;

import lombok.Data;

@Data
public class DeductionSearchDto {
	private String deductionId;
	private String deductionName;
	private String deductionCode;
	private String deductionDesc;
	private String searchKey;
}
