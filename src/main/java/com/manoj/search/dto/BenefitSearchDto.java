package com.manoj.search.dto;

import lombok.Data;

@Data
public class BenefitSearchDto {
	private String benefitId;
	private String benefitCode;
	private String benefitDesc;
	private String searchKey;
}
