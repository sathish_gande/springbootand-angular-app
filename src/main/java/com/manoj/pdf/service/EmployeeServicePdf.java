package com.manoj.pdf.service;

import java.io.ByteArrayOutputStream;

import org.springframework.stereotype.Service;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.manoj.export.model.constant.EmployeeConsPDF;
import com.manoj.export.model.constant.HeaderFooterPageEvent;
import com.manoj.model.Employee;

@Service
public class EmployeeServicePdf {
	
	public byte[] getContent(Employee employee, String empName) throws DocumentException {
		
		Document document=new Document();
		document.setMargins(EmployeeConsPDF.ZERO, EmployeeConsPDF.ZERO, EmployeeConsPDF.SIXTY, EmployeeConsPDF.SIXTY);
		ByteArrayOutputStream out= new ByteArrayOutputStream();
	
		PdfPTable table =new PdfPTable(EmployeeConsPDF.TWO);
		table.setWidths(new int[] {EmployeeConsPDF.TWO,EmployeeConsPDF.ONE});
		
		PdfPCell cell=new PdfPCell();
		cell.setBorder(EmployeeConsPDF.ZERO);
		cell.setPadding(EmployeeConsPDF.TEN);
		
		cell.setPhrase(new Phrase("Employee Id:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmpId()));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Employee Firrst Name:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmpFirstName()));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Employee Midle Name:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmpMidleName()));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Employee Last Name:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmplLastName()));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Employee Gender:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmpGender()));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Employee Type:"));
		table.addCell(cell);
		cell.setPhrase(new Phrase(employee.getEmpType()));
		table.addCell(cell);
		
		PdfWriter writer= PdfWriter.getInstance(document, out);
		HeaderFooterPageEvent event=new HeaderFooterPageEvent(empName);
		writer.setPageEvent(event);
		document.open();
		document.add(table);
		document.close();
		return out.toByteArray();
		
	}

}
