package com.manoj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "HR001")
@Where(clause="is_deleted=0")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEPINDX")
	private int id;

	@Column(name = "DEPID")
	private String depeartmentId;

	@Column(name = "DEPNAME")
	private String departmentName;

	@Column(name = "is_deleted", columnDefinition = "tinyint(0) default 0")
	protected Boolean isDeleted;

}
