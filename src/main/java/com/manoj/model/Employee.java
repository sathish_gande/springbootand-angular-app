package com.manoj.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.Data;

@Data
@Entity
@Table(name = "HR002")
@Where(clause = "is_deleted=0")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INDEXID")
	private int id;
	@Column(name = "EMPID")
	private String empId;
	@Column(name = "EMPFIRTNAME")
	private String empFirstName;
	@Column(name = "EMPMIDENAME")
	private String empMidleName;
	@Column(name = "EMPLASTNAME")
	private String emplLastName;
	@Column(name = "EMPGENDER")
	private String empGender;
	@Column(name = "EMPTYPE")
	private Integer empType;
	@Column(name = "is_deleted")
	protected Boolean isDeleted;

	@ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name = "DEPINDX")
	private Department department;
}
