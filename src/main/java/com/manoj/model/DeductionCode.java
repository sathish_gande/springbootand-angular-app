package com.manoj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "HR005")
public class DeductionCode {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEDINDEXID")
	private int id;

	@Column(name = "DEDCNID")
	private String deductionId;
	@Column(name = "DEDCNNMAE")
	private String deductionName;
	@Column(name = "DEDCNCODE")
	private String deductionCode;
	@Column(name = "DEDCNDESC")
	private String deductionDesc;
	@Column(name = "is_deleted")
	private boolean isDeleted;

}
