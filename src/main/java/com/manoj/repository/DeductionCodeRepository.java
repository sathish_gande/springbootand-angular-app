package com.manoj.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manoj.model.DeductionCode;

@Repository
public interface DeductionCodeRepository
		extends JpaRepository<DeductionCode, Integer>, JpaSpecificationExecutor<DeductionCode> {

	@Query("select d from DeductionCode d where d.id =:id")
	public DeductionCode findOne(@Param("id") Integer id);

	@Transactional
	@Query("update DeductionCode d set d.isDeleted =:deleted where d.id =:id")
	public void deleteById(@Param("deleted")boolean deleted,@Param("id") int id);

}
