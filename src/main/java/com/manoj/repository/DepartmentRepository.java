package com.manoj.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manoj.model.Department;

@Repository("/departmentRepository")
public interface DepartmentRepository extends JpaRepository<Department, Integer>, JpaSpecificationExecutor<Department> {

	@Query("select d from Department d where d.id=:id")
	public Department findOne(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Department d set d.isDeleted =:deleted  where d.id =:id ")
	public void deleteSingleRecord(@Param("deleted") boolean deleted, @Param("id") Integer id);

}
