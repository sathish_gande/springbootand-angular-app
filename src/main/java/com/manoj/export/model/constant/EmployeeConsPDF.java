package com.manoj.export.model.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EmployeeConsPDF {

	public static final float BORDER_WIDTH = 0.1f;
	public static final String HMP = "HMP";
	public static final char UNDERSCORE = '_';
	public static final String PDF_EXTENION = ".pdf";
	public static final Integer ZERO = 0;
	public static final Integer ONE = 1;
	public static final Integer TWO = 2;
	public static final Integer THREE = 3;
	public static final Integer FOUR = 4;
	public static final Integer FIVE = 5;
	public static final Integer SIX = 6;
	public static final Integer SEVEN = 7;
	public static final Integer EIGHT = 8;
	public static final Integer NINE = 9;
	public static final Integer TEN = 10;
	public static final Integer THIRTY = 30;
	public static final Integer SIXTY = 60;
	public static final long HUNDRED = 100;
	public static final long EIGHT_HUNDRED = 800;
	public static final Integer FIVE_FIFTY = 550;

}
