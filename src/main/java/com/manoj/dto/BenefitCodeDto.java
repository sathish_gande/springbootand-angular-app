package com.manoj.dto;

import java.util.List;

import lombok.Data;

@Data
public class BenefitCodeDto {

	private Integer id;
	private String benefitId;
	private String benefitCode;
	private String benefitDesc;
	private List<Integer> ids;
	private boolean isDeleted;
}
