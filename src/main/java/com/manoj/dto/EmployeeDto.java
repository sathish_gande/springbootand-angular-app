package com.manoj.dto;

import lombok.Data;

@Data
public class EmployeeDto {

	private Integer id;
	private String empId;
	private String empFirstName;
	private String empMidleName;
	private String emplLastName;
	private String empGender;
	private Integer empType;
	protected Boolean isDeleted;
	private DtoDepartment department;
	private Integer ids;

}
