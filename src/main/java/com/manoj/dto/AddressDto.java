package com.manoj.dto;

import java.util.List;

import lombok.Data;

@Data
public class AddressDto {

	private Integer id;
	private String countryId;
	private String countryName;
	private String cityName;
	private String stateName;
	private Integer zepCode;
	private boolean isDeleted;
	private List<Integer> ids; 
}
