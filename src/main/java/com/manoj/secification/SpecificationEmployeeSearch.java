package com.manoj.secification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.manoj.model.Employee;
import com.manoj.search.dto.EmployeeSearchDto;

public class SpecificationEmployeeSearch implements Specification<Employee>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private EmployeeSearchDto searchDto;

	public SpecificationEmployeeSearch(EmployeeSearchDto searchDto) {
		this.searchDto = searchDto;
	}



	@Override
	public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();
		List<Predicate>orPredicates=new  ArrayList<Predicate>();
		Predicate[] predicatesArray = new Predicate[predicates.size()];
		Predicate and=criteriaBuilder.and(predicates.toArray(predicatesArray));
		Predicate finalPredicate=null;
		
		if(searchDto.getSearchKey()!=null) {
			orPredicates.add(criteriaBuilder.like(root.get("empId"), "%" + searchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("empFirstName"), "%" + searchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("empMidleName"), "%" + searchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("emplLastName"), "%" + searchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("empGender"), "%" + searchDto.getSearchKey() + "%"));
			Predicate[] orPredicateArray= new Predicate[predicates.size()];
			Predicate or=criteriaBuilder.or(orPredicates.toArray(orPredicateArray));
			finalPredicate=criteriaBuilder.and(and,or);
		}else {
			finalPredicate=and;
		}
		return criteriaBuilder.and(finalPredicate);
	}

}
