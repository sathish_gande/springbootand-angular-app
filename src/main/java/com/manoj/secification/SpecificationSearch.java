package com.manoj.secification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.manoj.model.Address;
import com.manoj.search.dto.AddressSearchDto;

public class SpecificationSearch implements Specification<Address>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AddressSearchDto searchDto;
	
	public SpecificationSearch(AddressSearchDto searchDto) {
		this.searchDto=searchDto;
	}
	
	@Override
	public Predicate toPredicate(Root<Address> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate>predicate =new ArrayList<Predicate>();
		List<Predicate>orPredicates =new ArrayList<Predicate>();
		Predicate[] predicateArray=new Predicate[predicate.size()];
		Predicate and=criteriaBuilder.and(predicate.toArray(predicateArray));
		Predicate finalPredicate=null;
		if(searchDto.getSearchKey()!=null) {
			orPredicates.add(criteriaBuilder.like(root.get("countryId"), "%"+searchDto.getSearchKey()+"%"));
			orPredicates.add(criteriaBuilder.like(root.get("countryName"), "%"+searchDto.getSearchKey()+"%"));
			orPredicates.add(criteriaBuilder.like(root.get("cityName"), "%"+searchDto.getSearchKey()+"%"));
			orPredicates.add(criteriaBuilder.like(root.get("stateName"), "%"+searchDto.getSearchKey()+"%"));
			Predicate[] orPredicateArray=new Predicate[predicate.size()];
			Predicate or=criteriaBuilder.or(orPredicates.toArray(orPredicateArray));
			finalPredicate=criteriaBuilder.and(and,or);
		}else {
			finalPredicate=and;
		}
		return criteriaBuilder.and(finalPredicate);
	}

}
