package com.manoj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootandAngularAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootandAngularAppApplication.class, args);
	}

}
