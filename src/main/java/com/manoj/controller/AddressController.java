package com.manoj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.AddressDto;
import com.manoj.search.dto.AddressSearchDto;
import com.manoj.service.AddressService;

@RestController
@RequestMapping(value = "/addressController")
//@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public AddressDto create(@RequestBody AddressDto addressDto) {
		return addressService.saveOrUpdate(addressDto);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public AddressDto update(@RequestBody AddressDto addressDto) {
		return addressService.saveOrUpdate(addressDto);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDown() {
		return addressService.getAllDropDown();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public AddressDto delete(@RequestBody AddressDto addressDto) {
		return addressService.delete(addressDto);
	}

	@RequestMapping(value = "/deleteList", method = RequestMethod.POST)
	public List<AddressDto> deleteList(@RequestBody AddressDto addressDto) {
		return addressService.deleteList(addressDto.getIds());
	}

	@RequestMapping(value = "/searchData", method = RequestMethod.POST)
	public Page<AddressDto> searchAllData(@RequestBody AddressSearchDto searchDto, Pageable pageable) {
		return addressService.searchAllData(searchDto, pageable);
	}
}
