package com.manoj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.DtoDepartment;
import com.manoj.search.dto.DepartmentSearchDto;
import com.manoj.service.DepartmentService;

@RestController
@RequestMapping(value = "/departmentController")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public DtoDepartment create(@RequestBody DtoDepartment dtoDepartment) {
		return departmentService.saveOrUpdate(dtoDepartment);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public DtoDepartment update(@RequestBody DtoDepartment dtoDepartment) {
		return departmentService.saveOrUpdate(dtoDepartment);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<DtoDepartment> getAllDropDown() {
		return departmentService.getAllDropDown();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public DtoDepartment delete(@RequestBody DtoDepartment dtoDepartment) {
		return departmentService.delete(dtoDepartment);
	}

	@RequestMapping(value = "/getAllDropDownId", method = RequestMethod.GET)
	public List<DtoDepartment> getAllDropDownId() {
		return departmentService.getAllDropDownId();
	}
	@RequestMapping(value="/searchDepartment",method=RequestMethod.POST)
	public Page<DtoDepartment> searchDepartment(@RequestBody DepartmentSearchDto departmentSearchDto,Pageable pageable){
		return departmentService.searchDepartment(departmentSearchDto,pageable);
	}
}
