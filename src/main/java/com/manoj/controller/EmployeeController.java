package com.manoj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;
import com.manoj.dto.EmployeeDto;
import com.manoj.export.dto.ExportpdfDto;
import com.manoj.search.dto.EmployeeSearchDto;
import com.manoj.service.EmployeeService;

@RestController
@RequestMapping(value = "/employeeController")
//@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public EmployeeDto create(@RequestBody EmployeeDto employeeDto) {
		return employeeService.saveOrUpdate(employeeDto);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public EmployeeDto update(@RequestBody EmployeeDto employeeDto) {
		return employeeService.saveOrUpdate(employeeDto);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<EmployeeDto> getAllDropDown() {
		return employeeService.getAllDropDown();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public EmployeeDto delete(@RequestBody EmployeeDto employeeDto) {
		return employeeService.delete(employeeDto);
	}

	@RequestMapping(value = "/deleteList", method = RequestMethod.POST)
	public List<EmployeeDto> deleteList(@RequestBody List<Integer> ids) {
		return employeeService.deleList(ids);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public EmployeeDto getById(@RequestBody EmployeeDto employeeDto) {
		return employeeService.getById(employeeDto);
	}

	@RequestMapping(value = "/searchEmployee", method = RequestMethod.POST)
	public Page<EmployeeDto> searchEmployee(@RequestBody EmployeeSearchDto searchDto, Pageable pageable) {
		return employeeService.searchEmployee(searchDto, pageable);
	}

	@RequestMapping(value = "/exportPdf", method = RequestMethod.POST)
	public ResponseEntity<Object> exportEmployee(@RequestBody EmployeeDto employeeDto) throws DocumentException {
		ExportpdfDto exportpdfDto= employeeService.exportPDF(employeeDto.getId());
		return new ResponseEntity<Object>(exportpdfDto.getContent(),HttpStatus.OK);
	}
}
