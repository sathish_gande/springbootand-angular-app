package com.manoj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.BenefitCodeDto;
import com.manoj.search.dto.BenefitSearchDto;
import com.manoj.service.BenefitCodeService;

@RestController
@RequestMapping(value = "/benefitCodeController")
public class BenefitCodeController {

	@Autowired
	private BenefitCodeService benefitCodeService;

	@RequestMapping(value = "/createBenefit", method = RequestMethod.POST)
	public BenefitCodeDto createBenefit(@RequestBody BenefitCodeDto benefitCodeDto) {
		return benefitCodeService.saveOrUpdate(benefitCodeDto);
	}

	@RequestMapping(value = "/updateBenefit", method = RequestMethod.POST)
	public BenefitCodeDto updateBenefit(@RequestBody BenefitCodeDto benefitCodeDto) {
		return benefitCodeService.saveOrUpdate(benefitCodeDto);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public BenefitCodeDto getById(@RequestBody BenefitCodeDto benefitCodeDto) {
		return benefitCodeService.getById(benefitCodeDto);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<BenefitCodeDto> getAllDropDown() {
		return benefitCodeService.getAllDropDown();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public BenefitCodeDto delete(@RequestBody BenefitCodeDto benefitCodeDto) {
		return benefitCodeService.delete(benefitCodeDto);
	}

	@RequestMapping(value = "/deleteList", method = RequestMethod.POST)
	public List<BenefitCodeDto> deleteList(@RequestBody BenefitCodeDto benefitCodeDto) {
		return benefitCodeService.deleteList(benefitCodeDto.getIds());
	}

	@RequestMapping(value = "/searchBenefit", method = RequestMethod.POST)
	public Page<BenefitCodeDto> searchBenefit(@RequestBody BenefitSearchDto benefitSearchDto, Pageable pageable) {
		return benefitCodeService.searchBenefit(benefitSearchDto, pageable);
	}
}
