package com.manoj.controller;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.DeductionCodeDto;
import com.manoj.search.dto.DeductionSearchDto;
import com.manoj.service.DeductionCodeService;

@RestController
@RequestMapping(value = "/deductionCodeController")
public class DeductionCodeController {

	private DeductionCodeService deductionCodeService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public DeductionCodeDto createDeductionCode(@RequestBody DeductionCodeDto deductionCodeDto) {
		return deductionCodeService.saveOrUpdate(deductionCodeDto);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public DeductionCodeDto updateDeductionCode(@RequestBody DeductionCodeDto deductionCodeDto) {
		return deductionCodeService.saveOrUpdate(deductionCodeDto);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public DeductionCodeDto getById(@RequestBody DeductionCodeDto deductionCodeDto) {
		return deductionCodeService.getById(deductionCodeDto);
	}

	@RequestMapping(value = "/getAllDeduction", method = RequestMethod.GET)
	public List<DeductionCodeDto> getAllDeduction() {
		return deductionCodeService.getAllDeduction();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public DeductionCodeDto delete(@RequestBody DeductionCodeDto deductionCodeDto) {
		return deductionCodeService.delete(deductionCodeDto);
	}

	@RequestMapping(value = "/getAllDeduction", method = RequestMethod.POST)
	public List<DeductionCodeDto> deleteList(@RequestBody List<Integer> ids) {
		return deductionCodeService.getAllDeduction(ids);
	}

	@RequestMapping(value = "/searchDeduction", method = RequestMethod.POST)
	public Page<DeductionCodeDto> searchDeduction(@RequestBody DeductionSearchDto searchDto, Pageable pageable) {
		return deductionCodeService.searchDeduction(searchDto, pageable);
	}
}
