package com.manoj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.manoj.dto.DtoDepartment;
import com.manoj.model.Department;
import com.manoj.repository.DepartmentRepository;
import com.manoj.search.dto.DepartmentSearchDto;
import com.manoj.secification.SpecificationDepartmentSearch;



@Service
public class DepartmentService {
	
	
	@Autowired
	private DepartmentRepository departmentRepository;

	public DtoDepartment saveOrUpdate(DtoDepartment dtoDepartment) {
		try {
			Department department = new Department();
			if (dtoDepartment.getId() != null && dtoDepartment.getId() > 0) {
				department.setId(dtoDepartment.getId());
				department.setDepeartmentId(dtoDepartment.getDepeartmentId());
				department.setDepartmentName(dtoDepartment.getDepartmentName());
				department.setIsDeleted(Boolean.FALSE);
				departmentRepository.saveAndFlush(department);
			} else {
				department.setDepeartmentId(dtoDepartment.getDepeartmentId());
				department.setDepartmentName(dtoDepartment.getDepartmentName());
				department.setIsDeleted(Boolean.FALSE);
				departmentRepository.saveAndFlush(department);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dtoDepartment;
	}

	public List<DtoDepartment> getAllDropDown() {
		List<DtoDepartment> dtoDepartmentsList = new ArrayList<>();
		List<Department> departmentList = departmentRepository.findAll();
		if (!departmentList.isEmpty()) {
			for (Department department : departmentList) {
					DtoDepartment dtoDepartment = new DtoDepartment();
					dtoDepartment.setId(department.getId());
					dtoDepartment.setDepartmentName(department.getDepartmentName());
					dtoDepartment.setDepeartmentId(department.getDepeartmentId());
					dtoDepartmentsList.add(dtoDepartment);
			}
		}

		return dtoDepartmentsList;
	}

	public DtoDepartment delete(DtoDepartment dtoDepartment) {
		Department department=departmentRepository.findOne(dtoDepartment.getId());
		if(department!=null) {
			dtoDepartment.setId(department.getId());
			departmentRepository.deleteSingleRecord(true,dtoDepartment.getId());
		}
		return dtoDepartment;
	}

	public List<DtoDepartment> getAllDropDownId() {
		List<DtoDepartment> dtoDepartmentsList = new ArrayList<>();
		List<Department> departmentList = departmentRepository.findAll();
		if (!departmentList.isEmpty()) {
			for (Department department : departmentList) {
					DtoDepartment dtoDepartment = new DtoDepartment();
					dtoDepartment.setId(department.getId());
					dtoDepartment.setDepeartmentId(department.getDepeartmentId());
					dtoDepartmentsList.add(dtoDepartment);
			}
		}
		return dtoDepartmentsList;
	}

	public Page<DtoDepartment> searchDepartment(DepartmentSearchDto searchDto, Pageable pageable) {
		   Page<Department> pageList=departmentRepository.findAll(new SpecificationDepartmentSearch(searchDto),pageable);
		   List<DtoDepartment>content=pageList.getContent().stream().map(this::getDTOData).collect(Collectors.toList());
		return new PageImpl<>(content,pageable,pageList.getTotalElements());
	}
	
	public DtoDepartment getDTOData(Department department) {
		DtoDepartment dtoDepartment=new DtoDepartment();
		dtoDepartment.setId(department.getId());
		dtoDepartment.setDepartmentName(department.getDepartmentName());
		dtoDepartment.setDepeartmentId(department.getDepeartmentId());
		return dtoDepartment;
	}

}