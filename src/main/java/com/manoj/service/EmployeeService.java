package com.manoj.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.lowagie.text.DocumentException;
import com.manoj.dto.DtoDepartment;
import com.manoj.dto.EmployeeDto;
import com.manoj.export.dto.ExportpdfDto;
import com.manoj.export.model.constant.EmployeeConsPDF;
import com.manoj.model.Department;
import com.manoj.model.Employee;
import com.manoj.pdf.service.EmployeeServicePdf;
import com.manoj.repository.DepartmentRepository;
import com.manoj.repository.EmployeeRepository;
import com.manoj.search.dto.EmployeeSearchDto;
import com.manoj.secification.SpecificationEmployeeSearch;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private EmployeeServicePdf servicePdf;

	public EmployeeDto saveOrUpdate(EmployeeDto employeeDto) {
		Employee employee = new Employee();
		if (employeeDto.getId() != null && employeeDto.getId() > 0) {

			Department department = null;
			if (employeeDto.getDepartment() != null && employeeDto.getDepartment().getId() > 0) {
				department = departmentRepository.findOne(employeeDto.getDepartment().getId());
			}
			employee.setDepartment(department);
			employee.setId(employeeDto.getId());
			employee.setEmpId(employeeDto.getEmpId());
			employee.setEmpFirstName(employeeDto.getEmpFirstName());
			employee.setEmpMidleName(employeeDto.getEmpMidleName());
			employee.setEmplLastName(employeeDto.getEmplLastName());
			employee.setEmpGender(employeeDto.getEmpGender());
			employee.setEmpType(employeeDto.getEmpType());
			employee.setIsDeleted(Boolean.FALSE);
			employeeRepository.saveAndFlush(employee);
		} else {
			Department department = new Department();
			if (employeeDto.getDepartment() != null && employeeDto.getDepartment().getId() > 0) {
				department = departmentRepository.findOne(employeeDto.getDepartment().getId());
			}
			employee.setDepartment(department);
			employee.setEmpId(employeeDto.getEmpId());
			employee.setEmpFirstName(employeeDto.getEmpFirstName());
			employee.setEmpMidleName(employeeDto.getEmpMidleName());
			employee.setEmplLastName(employeeDto.getEmplLastName());
			employee.setEmpGender(employeeDto.getEmpGender());
			employee.setEmpType(employeeDto.getEmpType());
			employee.setIsDeleted(Boolean.FALSE);
			employeeRepository.saveAndFlush(employee);
		}
		return employeeDto;
	}

	@Transactional
	public List<EmployeeDto> getAllDropDown() {
		List<EmployeeDto> employeeDtoList = new ArrayList<EmployeeDto>();
		List<Employee> employeeList = employeeRepository.findAll();
		if (employeeList != null && !employeeList.isEmpty()) {
			for (Employee employee : employeeList) {
				EmployeeDto employeeDto = new EmployeeDto();
				DtoDepartment dtoDepartment = new DtoDepartment();
				if (employee.getDepartment() != null) {
					dtoDepartment.setId(employee.getDepartment().getId());
					dtoDepartment.setDepartmentName(employee.getDepartment().getDepartmentName());
					dtoDepartment.setDepeartmentId(employee.getDepartment().getDepeartmentId());
					employeeDto.setDepartment(dtoDepartment);
				}
				employeeDto.setId(employee.getId());
				employeeDto.setEmpId(employee.getEmpId());
				employeeDto.setEmpFirstName(employee.getEmpFirstName());
				employeeDto.setEmpMidleName(employee.getEmpMidleName());
				employeeDto.setEmplLastName(employee.getEmplLastName());
				employeeDto.setEmpGender(employee.getEmpGender());
				employeeDto.setEmpType(employee.getEmpType());
				employeeDto.setIsDeleted(employee.getIsDeleted());
				employeeDtoList.add(employeeDto);
			}
		}
		return employeeDtoList;
	}

	public EmployeeDto delete(EmployeeDto employeeDto) {
		Employee employee = employeeRepository.findOne(employeeDto.getId());
		if (employee != null) {
			employeeDto.setId(employee.getId());
			employeeRepository.deleteById(true, employeeDto.getId());
		}
		return employeeDto;
	}

	public EmployeeDto getById(EmployeeDto employeeDto) {
		Employee employee = employeeRepository.findOne(employeeDto.getId());
		if (employee != null) {
			employeeDto.setId(employee.getId());
			employeeDto.setEmpId(employee.getEmpId());
			employeeDto.setEmpFirstName(employee.getEmpFirstName());
			employeeDto.setEmpMidleName(employee.getEmpMidleName());
			employeeDto.setEmplLastName(employee.getEmplLastName());
			employeeDto.setEmpGender(employee.getEmpGender());
			employeeDto.setEmpType(employee.getEmpType());
			employeeDto.setIsDeleted(employee.getIsDeleted());
		}
		return employeeDto;
	}

	public List<EmployeeDto> deleList(List<Integer> ids) {
		List<EmployeeDto> employeeDtosList = new ArrayList<>();
		for (Integer id : ids) {
			Employee employee = employeeRepository.findOne(id);
			if (employee != null) {
				EmployeeDto employeeDto = new EmployeeDto();
				employeeDto.setId(employee.getId());
				employeeDto.setEmpId(employee.getEmpId());
				employeeRepository.deleSingleRecord(true, id);
				employeeDtosList.add(employeeDto);
			}
		}
		return employeeDtosList;
	}

	public Page<EmployeeDto> searchEmployee(EmployeeSearchDto searchDto, Pageable pageable) {
		Page<Employee> pageList = employeeRepository.findAll(new SpecificationEmployeeSearch(searchDto), pageable);
		List<EmployeeDto> content = pageList.getContent().stream().map(this::getDTO).collect(Collectors.toList());
		return new PageImpl<>(content, pageable, pageList.getTotalElements());
	}

	public EmployeeDto getDTO(Employee employee) {
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setId(employee.getId());
		employeeDto.setEmpId(employee.getEmpId());
		employeeDto.setEmpFirstName(employee.getEmpFirstName());
		employeeDto.setEmpMidleName(employee.getEmpMidleName());
		employeeDto.setEmplLastName(employee.getEmplLastName());
		employeeDto.setEmpGender(employee.getEmpGender());
		employeeDto.setEmpType(employee.getEmpType());
		employeeDto.setIsDeleted(employee.getIsDeleted());
		return employeeDto;

	}

	public ExportpdfDto exportPDF(Integer id) throws DocumentException {
		
		ExportpdfDto exportpdfDto=new ExportpdfDto();
		exportpdfDto.setFileType(MediaType.APPLICATION_PDF_VALUE);
		Employee employee=employeeRepository.findOne(id);
		exportpdfDto.setFileName(getFileName("null"));
		exportpdfDto.setContent(servicePdf.getContent(employee,employee.getEmpFirstName()));
		return exportpdfDto;
	}
	
	private String getFileName(String empName) {
		String fileName = empName.replace(EmployeeConsPDF.HMP, "");
		StringBuilder builder = new StringBuilder();
		builder.append(fileName.subSequence(0, 3).toString().toUpperCase());
		builder.append(EmployeeConsPDF.UNDERSCORE);
		builder.append("Employee_Details");
		DateFormat format = new SimpleDateFormat("ddMMyyy");
		String date = format.format(new Date());
		builder.append(date);
		builder.append(EmployeeConsPDF.PDF_EXTENION);
		return builder.toString();
		
	}

}
