package com.manoj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.manoj.dto.DeductionCodeDto;
import com.manoj.model.DeductionCode;
import com.manoj.repository.DeductionCodeRepository;
import com.manoj.search.dto.DeductionSearchDto;
import com.manoj.secification.SpecificationDeductonSearch;

@Service
public class DeductionCodeService {

	private DeductionCodeRepository deductionCodeRepository;

	public DeductionCodeDto saveOrUpdate(DeductionCodeDto deductionCodeDto) {

		DeductionCode deductionCode = new DeductionCode();
		if (deductionCodeDto.getId() != null && deductionCodeDto.getId() > 0) {
			deductionCode.setId(deductionCodeDto.getId());
			deductionCode.setDeductionCode(deductionCodeDto.getDeductionCode());
			deductionCode.setDeductionId(deductionCodeDto.getDeductionId());
			deductionCode.setDeductionName(deductionCodeDto.getDeductionName());
			deductionCode.setDeductionDesc(deductionCodeDto.getDeductionDesc());
			deductionCode.setDeleted(Boolean.FALSE);
			deductionCodeRepository.saveAndFlush(deductionCode);
		} else {
			deductionCode.setDeductionCode(deductionCodeDto.getDeductionCode());
			deductionCode.setDeductionId(deductionCodeDto.getDeductionId());
			deductionCode.setDeductionName(deductionCodeDto.getDeductionName());
			deductionCode.setDeductionDesc(deductionCodeDto.getDeductionDesc());
			deductionCode.setDeleted(Boolean.FALSE);
			deductionCodeRepository.saveAndFlush(deductionCode);
		}
		return deductionCodeDto;
	}

	public DeductionCodeDto getById(DeductionCodeDto deductionCodeDto) {
		DeductionCode deductionCode = deductionCodeRepository.findOne(deductionCodeDto.getId());
		if (deductionCode != null) {
			deductionCodeDto.setId(deductionCode.getId());
			deductionCodeDto.setDeductionCode(deductionCode.getDeductionCode());
			deductionCodeDto.setDeductionId(deductionCode.getDeductionId());
			deductionCodeDto.setDeductionName(deductionCode.getDeductionName());
			deductionCodeDto.setDeductionDesc(deductionCode.getDeductionDesc());
			deductionCodeDto.setDeleted(deductionCode.isDeleted());
		}
		return deductionCodeDto;
	}

	public List<DeductionCodeDto> getAllDeduction() {
		List<DeductionCodeDto> deductionCodeDtosList = new ArrayList<DeductionCodeDto>();
		List<DeductionCode> deductionCodesList = deductionCodeRepository.findAll();
		if (deductionCodesList != null && !deductionCodesList.isEmpty()) {
			for (DeductionCode deductionCode : deductionCodesList) {
				DeductionCodeDto deductionCodeDto = new DeductionCodeDto();
				deductionCodeDto.setId(deductionCode.getId());
				deductionCodeDto.setDeductionCode(deductionCode.getDeductionCode());
				deductionCodeDto.setDeductionId(deductionCode.getDeductionId());
				deductionCodeDto.setDeductionName(deductionCode.getDeductionName());
				deductionCodeDto.setDeductionDesc(deductionCode.getDeductionDesc());
				deductionCodeDto.setDeleted(deductionCode.isDeleted());
				deductionCodeDtosList.add(deductionCodeDto);
			}
		}
		return deductionCodeDtosList;
	}

	public DeductionCodeDto delete(DeductionCodeDto deductionCodeDto) {
		DeductionCode deductionCode = deductionCodeRepository.findOne(deductionCodeDto.getId());
		if (deductionCode != null) {
			deductionCodeDto.setId(deductionCode.getId());
			deductionCodeRepository.deleteById(true, deductionCode.getId());
		}
		return deductionCodeDto;
	}

	public List<DeductionCodeDto> getAllDeduction(List<Integer> ids) {
		List<DeductionCodeDto> deductionCodeDtosList = new ArrayList<>();
		for (Integer id : ids) {
			DeductionCode deductionCode = deductionCodeRepository.findOne(id);
			if (deductionCode != null) {
				DeductionCodeDto deductionCodeDto = new DeductionCodeDto();
				deductionCodeDto.setId(deductionCode.getId());
				deductionCodeRepository.deleteById(true, deductionCode.getId());
				deductionCodeDtosList.add(deductionCodeDto);
			}
		}
		return deductionCodeDtosList;
	}

	public Page<DeductionCodeDto> searchDeduction(DeductionSearchDto searchDto, Pageable pageable) {
			Page<DeductionCode> pageList =deductionCodeRepository.findAll(new SpecificationDeductonSearch(searchDto), pageable);
			List<DeductionCodeDto> contents=pageList.getContent().stream().map(this::getDTO).collect(Collectors.toList());
		return new PageImpl<>(contents,pageable,pageList.getTotalElements());
	}
	
	public DeductionCodeDto getDTO(DeductionCode  deductionCode) {
		DeductionCodeDto deductionCodeDto = new DeductionCodeDto();
		deductionCodeDto.setId(deductionCode.getId());
		deductionCodeDto.setDeductionCode(deductionCode.getDeductionCode());
		deductionCodeDto.setDeductionId(deductionCode.getDeductionId());
		deductionCodeDto.setDeductionName(deductionCode.getDeductionName());
		deductionCodeDto.setDeductionDesc(deductionCode.getDeductionDesc());
		deductionCodeDto.setDeleted(deductionCode.isDeleted());
		return deductionCodeDto;
	}

}
