package com.manoj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.manoj.dto.AddressDto;
import com.manoj.model.Address;
import com.manoj.repository.AddressRepository;
import com.manoj.search.dto.AddressSearchDto;
import com.manoj.secification.SpecificationSearch;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	public AddressDto saveOrUpdate(AddressDto addressDto) {
		Address address = new Address();
		if (addressDto.getId() != null && address.getId() > 0) {
			address.setId(addressDto.getId());
			address.setCityName(addressDto.getCityName());
			address.setCountryId(addressDto.getCountryId());
			address.setCountryName(addressDto.getCountryName());
			address.setStateName(addressDto.getStateName());
			address.setZepCode(addressDto.getZepCode());
			address.setDeleted(Boolean.FALSE);
			addressRepository.saveAndFlush(address);
		} else {
			address.setCityName(addressDto.getCityName());
			address.setCountryId(addressDto.getCountryId());
			address.setCountryName(addressDto.getCountryName());
			address.setStateName(addressDto.getStateName());
			address.setZepCode(addressDto.getZepCode());
			address.setDeleted(Boolean.FALSE);
			addressRepository.saveAndFlush(address);
		}
		return addressDto;
	}

	public List<AddressDto> getAllDropDown() {
		List<AddressDto> addressDtoList = new ArrayList<AddressDto>();
		List<Address> addressList = addressRepository.findAll();
		if (addressList != null && !addressList.isEmpty()) {
			for (Address address : addressList) {
				AddressDto addressDto = new AddressDto();
				addressDto.setId(address.getId());
				addressDto.setCityName(address.getCityName());
				addressDto.setCountryId(address.getCountryId());
				addressDto.setCountryName(address.getCountryName());
				addressDto.setStateName(address.getStateName());
				addressDto.setZepCode(address.getZepCode());
				addressDto.setDeleted(address.isDeleted());
				addressDtoList.add(addressDto);
			}
		}
		return addressDtoList;
	}

	public AddressDto delete(AddressDto addressDto) {
		Address address = addressRepository.findOne(addressDto.getId());
		if (address != null) {
			addressDto.setId(address.getId());
			addressRepository.deleteById(true, address.getId());
		}
		return addressDto;
	}

	/*public Page<AddressDto> searchAllData(AddressSearchDto searchDto, Pageable pageable) {
		Page<Address> addressList = addressRepository.searchData(searchDto.getSearchKey(), pageable);
		List<AddressDto>content=addressList.getContent().stream().map(this::toDto).collect(Collectors.toList());
		return new PageImpl<>(content,pageable,addressList.getTotalElements());
	}*/
	public Page<AddressDto> searchAllData(AddressSearchDto searchDto, Pageable pageable) {
		Page<Address> addressList = addressRepository.findAll(new SpecificationSearch(searchDto), pageable);
		List<AddressDto>content=addressList.getContent().stream().map(this::toDto).collect(Collectors.toList());
		return new PageImpl<>(content,pageable,addressList.getTotalElements());
	}
	public AddressDto toDto(Address address) {
		AddressDto addressDto=new AddressDto();
		addressDto.setId(address.getId());
		addressDto.setCityName(address.getCityName());
		addressDto.setCountryId(address.getCountryId());
		addressDto.setCountryName(address.getCountryName());
		addressDto.setStateName(address.getStateName());
		addressDto.setZepCode(address.getZepCode());
		addressDto.setDeleted(address.isDeleted());
		return addressDto;
	}

	public List<AddressDto> deleteList(List<Integer> ids) {
		List<AddressDto> addressDtosList=new ArrayList<>();
		for (Integer id: ids) {
			Address address=addressRepository.findOne(id);
			AddressDto addressDto=new AddressDto();
			addressDto.setCountryId(address.getCountryId());
			addressRepository.deleteSinglerecord(true,address.getId());
			addressDtosList.add(addressDto);
		}
		
		return addressDtosList;
	}

}
