package com.manoj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.manoj.dto.BenefitCodeDto;
import com.manoj.model.BenefitCode;
import com.manoj.repository.BenefitCodeRepository;
import com.manoj.search.dto.BenefitSearchDto;
import com.manoj.secification.SpecificationBenefitSeaarch;

@Service
public class BenefitCodeService {

	@Autowired
	private BenefitCodeRepository benefitCodeRepository;

	public BenefitCodeDto saveOrUpdate(BenefitCodeDto benefitCodeDto) {
		BenefitCode benefitCode = new BenefitCode();
		if (benefitCodeDto.getId() != null && benefitCodeDto.getId() > 0) {
			benefitCode.setId(benefitCodeDto.getId());
			benefitCode.setBenefitCode(benefitCodeDto.getBenefitCode());
			benefitCode.setBenefitId(benefitCodeDto.getBenefitId());
			benefitCode.setBenefitDesc(benefitCodeDto.getBenefitDesc());
			benefitCodeRepository.saveAndFlush(benefitCode);
		} else {
			benefitCode.setBenefitCode(benefitCodeDto.getBenefitCode());
			benefitCode.setBenefitId(benefitCodeDto.getBenefitId());
			benefitCode.setBenefitDesc(benefitCodeDto.getBenefitDesc());
			benefitCodeRepository.saveAndFlush(benefitCode);
		}
		return benefitCodeDto;
	}

	public BenefitCodeDto getById(BenefitCodeDto benefitCodeDto) {
		BenefitCode benefitCode = benefitCodeRepository.findOne(benefitCodeDto.getId());
		if (benefitCode != null) {
			benefitCodeDto.setId(benefitCode.getId());
			benefitCodeDto.setBenefitCode(benefitCode.getBenefitCode());
			benefitCodeDto.setBenefitId(benefitCode.getBenefitId());
			benefitCodeDto.setBenefitDesc(benefitCode.getBenefitDesc());
		}
		return benefitCodeDto;
	}

	public List<BenefitCodeDto> getAllDropDown() {
		List<BenefitCodeDto> benefitCodeDtoList = new ArrayList<BenefitCodeDto>();
		List<BenefitCode> benefitCodeList = benefitCodeRepository.findAll();
		if (benefitCodeList != null && !benefitCodeList.isEmpty()) {
			for (BenefitCode benefitCode : benefitCodeList) {
				BenefitCodeDto benefitCodeDto = new BenefitCodeDto();
				benefitCodeDto.setId(benefitCode.getId());
				benefitCodeDto.setBenefitCode(benefitCode.getBenefitCode());
				benefitCodeDto.setBenefitId(benefitCode.getBenefitId());
				benefitCodeDto.setBenefitDesc(benefitCode.getBenefitDesc());
				benefitCodeDtoList.add(benefitCodeDto);
			}
		}
		return benefitCodeDtoList;
	}

	public BenefitCodeDto delete(BenefitCodeDto benefitCodeDto) {
		BenefitCode benefitCode = benefitCodeRepository.findOne(benefitCodeDto.getId());
		if (benefitCode != null) {
			benefitCodeDto.setBenefitId(benefitCode.getBenefitId());
			benefitCodeDto.setId(benefitCode.getId());
			benefitCodeRepository.deleteById(true, benefitCodeDto.getId());
		}
		return benefitCodeDto;
	}

	public List<BenefitCodeDto> deleteList(List<Integer> ids) {
		List<BenefitCodeDto> benefitCodeDtoList = new ArrayList<>();
		for (Integer id : ids) {
			BenefitCode benefitCode = benefitCodeRepository.findOne(id);
			if (benefitCode != null) {
				BenefitCodeDto benefitCodeDto = new BenefitCodeDto();
				benefitCodeDto.setId(benefitCode.getId());
				benefitCodeDto.setBenefitId(benefitCode.getBenefitId());
				benefitCodeRepository.deleteSingleBenefitCode(true, id);
				benefitCodeDtoList.add(benefitCodeDto);
			}
		}
		return benefitCodeDtoList;
	}

	public Page<BenefitCodeDto> searchBenefit(BenefitSearchDto benefitSearchDto, Pageable pageable) {
		Page<BenefitCode>pageList=benefitCodeRepository.findAll(new SpecificationBenefitSeaarch(benefitSearchDto), pageable);
		List<BenefitCodeDto> content=pageList.getContent().stream().map(this::getDTO).collect(Collectors.toList());
		return new PageImpl<>(content,pageable,pageList.getTotalElements());
	}
	
	public BenefitCodeDto getDTO(BenefitCode benefitCode) {
		BenefitCodeDto benefitCodeDto=new BenefitCodeDto();
		benefitCodeDto.setBenefitCode(benefitCode.getBenefitCode());
		benefitCodeDto.setBenefitDesc(benefitCode.getBenefitDesc());
		benefitCodeDto.setId(benefitCode.getId());
		return benefitCodeDto;
		
	}
}
